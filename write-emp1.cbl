       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-EMP1.
       AUTHOR. PAKAWAT.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD EMP-FILE.
       01 EmployeeDetails.
           88 END-OF-EMP-FILE VALUE HIGH-VALUES.
           05 EMP-SSN   PIC   9(9).
           05 EMP-NAME.
            10 EMP-SURNAME PIC X(15).
            10 EMP-FORENAME PIC X(10).
           05 EMP-DATE-OF-BIRTH.
            10 EMP-YOB PIC 9(4).
            10 EMP-MOB PIC 99.
            10 EMP-DOB PIC 99.  
           05 EMP-GENDER  PIC X.


       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT  EMP-FILE 
              MOVE "123456789" TO EMP-SSN 
              MOVE "PAKAWAT" TO   EMP-FORENAME
              MOVE "JAROENYING" TO EMP-SURNAME
              MOVE 20001225 TO EMP-DATE-OF-BIRTH
              MOVE "M" TO EMP-GENDER
              WRITE EmployeeDetails 
   
              MOVE "987654321" TO EMP-SSN 
              MOVE "JAKKARIN" TO EMP-FORENAME
              MOVE "SUKSAWASCHON" TO EMP-SURNAME
              MOVE 19750110 TO EMP-DATE-OF-BIRTH
              MOVE "M" TO EMP-GENDER
              WRITE EmployeeDetails 
   
              MOVE "999999999" TO EMP-SSN 
              MOVE "UREERAT" TO EMP-FORENAME
              MOVE "SUKSAWASCHON" TO EMP-SURNAME
              MOVE 19750413 TO EMP-DATE-OF-BIRTH
              MOVE "F" TO EMP-GENDER
              WRITE EmployeeDetails 
           CLOSE EMP-FILE 
           GOBACK 
       .