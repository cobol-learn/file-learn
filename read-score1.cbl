       IDENTIFICATION DIVISION. 
       PROGRAM-ID. READ-SCORE1.
       AUTHOR. PAKAWAT.


       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO  "score.dat"
              ORGANIZATION   IS LINE  SEQUENTIAL.

           SELECT GRADE-FILE ASSIGN TO "grade.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       

       DATA DIVISION. 
       FILE SECTION. 
       FD SCORE-FILE.
       01 SCORE-DETAIL.
        88  END-OF-FILE VALUE HIGH-VALUES.
        05 STU-ID PIC 9(8).
        05 MIDTERM-SCORE PIC  9(2)V9(2).
        05 FINAL-SCORE PIC  9(2)V9(2).
        05 PROJECT-SCORE PIC  9(2)V9(2).
       FD GRADE-FILE.
       01 GRADE-DETAIL.
        05 STU-ID  PIC 9(8).
        05  SUM-SCORE   PIC   9(3)V9(2).
        05  GRADE PIC   X(2).


       
       PROCEDURE DIVISION .

       000-BEGIN.
           OPEN OUTPUT GRADE-FILE 
           OPEN INPUT SCORE-FILE
           
           PERFORM UNTIL END-OF-FILE 
              READ  SCORE-FILE 
                 AT END SET END-OF-FILE TO TRUE  
              END-READ
              IF NOT END-OF-FILE  
                 PERFORM 001-PROCESS THRU 001-EXIT
              END-IF
           END-PERFORM

           CLOSE SCORE-FILE 
           CLOSE GRADE-FILE

           GOBACK 
       .


       001-PROCESS.
           COMPUTE SUM-SCORE = MIDTERM-SCORE + FINAL-SCORE +
               PROJECT-SCORE 

           PERFORM 002-CALCULATE-GRADE THRU 002-EXIT 
         
       .

       001-EXIT.
           EXIT
       .

       002-CALCULATE-GRADE.
            EVALUATE TRUE 
              WHEN  SUM-SCORE >=  80 MOVE "A" TO GRADE 
              WHEN  SUM-SCORE >=  75 MOVE "B+" TO GRADE 
              WHEN  SUM-SCORE >=  70 MOVE "B" TO GRADE 
              WHEN  SUM-SCORE >=  65 MOVE "C+" TO GRADE 
              WHEN  SUM-SCORE >=  60 MOVE "C" TO GRADE 
              WHEN  SUM-SCORE >=  55 MOVE "D+" TO GRADE 
              WHEN  SUM-SCORE >=  50 MOVE "D" TO GRADE 
              WHEN OTHER MOVE "F" TO GRADE
           END-EVALUATE
       .


       002-WRITE-TO-GRADE-FILE.
           MOVE STU-ID IN SCORE-DETAIL TO  STU-ID IN GRADE-DETAIL 
           WRITE GRADE-DETAIL
       .

       002-EXIT.
           EXIT
       .
