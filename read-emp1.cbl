       IDENTIFICATION DIVISION.   
       PROGRAM-ID. READ-EMP1.
       AUTHOR. PAKAWAT.


       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT  EMP-FILE ASSIGN TO "emp1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD EMP-FILE.
       01 EmployeeDetails.
           88 END-OF-EMP-FILE VALUE HIGH-VALUES.
           05 EMP-SSN   PIC   9(9).
           05 EMP-NAME.
            10 EMP-SURNAME PIC X(15).
            10 EMP-FORENAME PIC X(10).
           05 EMP-DATE-OF-BIRTH.
            10 EMP-YOB PIC 9(4).
            10 EMP-MOB PIC 99.
            10 EMP-DOB PIC 99.  
           05 EMP-GENDER  PIC X.


       PROCEDURE DIVISION .
       000-BEGIN.

           OPEN INPUT EMP-FILE 
            DISPLAY "======================================="
           
            PERFORM UNTIL END-OF-EMP-FILE
              READ EMP-FILE
                AT END SET END-OF-EMP-FILE TO TRUE
              END-READ
              IF NOT END-OF-EMP-FILE
                 DISPLAY "SSN: "EMP-SSN 
                 DISPLAY "NAME: "EMP-FORENAME " " EMP-SURNAME  
                 DISPLAY "DOB:  " EMP-YOB "/" EMP-MOB "/" EMP-DOB
                 DISPLAY "GENDER: "EMP-GENDER 
                 DISPLAY "======================================="
              END-IF
            END-PERFORM

           CLOSE EMP-FILE
           
           .